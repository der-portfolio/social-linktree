# social-linktree

This is my personal link page to my social media profiles. It serves a similar purpose as e.g. LinkTree, except that it's selfhosted. The live version can be found at [nostradamo.netlify.app/](https://nostradamo.netlify.app/).

[![Netlify Status](https://api.netlify.com/api/v1/badges/592241b1-9608-4e6a-a9d5-3d94a4f43764/deploy-status)](https://app.netlify.com/sites/nostradamo/deploys)

## Authors

- [Oliver Traber](https://github.com/BluemediaGER)  - *Initial work*
- [Damian Ritorto](https://github.com/D4mo)  - *Updates*

## License

This project is licensed under the GNU General Public License v3.0 - see the [LICENSE.md](LICENSE.md) file for details
